from setuptools import setup

setup(name='optical_rl_gym',
      version='0.0.1-alpha',
      install_requires=['tensorflow==1.15', 'stable_baselines[mpi]', 'gym', 'numpy', 'matplotlib',
                        'networkx']
      )
